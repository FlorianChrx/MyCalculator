package comptability;

import java.io.Serializable;

public class Account implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1318078170243040222L;

	private double amount;

	public double getAmount() {
		return amount;
	}
	
	public void reset() {
		this.amount = 0;
	}
	
	public boolean deposit(double d) {
		this.amount += d;
		return true;
	}
	
	public boolean withdraw(double d) {
		this.amount -= d;
		return true;
	}
	
}
