package iface;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import comptability.Account;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class Controller {
	@FXML
	private Label counter;
	@FXML
	private Label wCounter;
	@FXML
	private Label dCounter;
	@FXML 
	private TextField txtField;
	private ObjectInputStream in;
	private ObjectOutputStream out;
	private Account account;
	
	public void initialize() {
		try {
			in = new ObjectInputStream(new FileInputStream(""));
			in.readObject();
			account = (Account) in.readObject();
		} catch (Exception e) {
			account = new Account();
		}
		refresh();
	}
	
	public void deposit() {
		account.deposit(toDouble(txtField.getText()));
		refresh();
	}
	
	public void withdraw() {
		account.withdraw(toDouble(txtField.getText()));
		refresh();
	}
	private double toDouble(String s){
		try {
			return Double.parseDouble(s);
		} catch (Exception e) {
			return 0;
		}
	}
	
	private void refresh() {
		counter.setText(account.getAmount()+"");
	}
	
	public void changes() {
		wCounter.setText((account.getAmount()-toDouble(txtField.getText()))+"");
		dCounter.setText((account.getAmount()+toDouble(txtField.getText()))+"");
	}
	
	@FXML
	public void exitApplication(ActionEvent event) throws IOException {
		out.writeObject(account);
		Platform.exit();
	}
}
